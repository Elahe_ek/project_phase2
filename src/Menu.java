import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import javax.swing.*;

import static java.lang.Integer.parseInt;

public class Menu extends JFrame {

    JLabel widthLabel ;
    JLabel heightLabel;
    JLabel enemyLabel;
    JTextField widthText;
    JTextField heightText;
    JTextField enemyText;
    JButton start;
    JButton load;

    Menu() {

        super("Bomber Man");
        JLabel background = new JLabel(new ImageIcon(new File("").getAbsolutePath()+"/images/y.jpg"));
        this.setContentPane(background);

        this.setLocation(400 , 150);
        getContentPane().setLayout(null);

        widthLabel = new JLabel("WIDTH :");
        widthLabel.setLocation(70 , 250);
        widthLabel.setSize(80 , 20);
        add(widthLabel);

        heightLabel = new JLabel("HEIGHT :");
        heightLabel.setLocation(70 , 300);
        heightLabel.setSize(80 , 20);
        add(heightLabel);

        enemyLabel = new JLabel("ENEMIES :");
        enemyLabel.setLocation(370 , 270);
        enemyLabel.setSize(80 , 20);
        add(enemyLabel);

        widthText = new JTextField();
        widthText.setLocation(140 , 250);
        widthText.setSize(60 , 20);
        widthText.setFont(new Font("Arial", Font.PLAIN, 20));
        add(widthText);

        heightText = new JTextField();
        heightText.setLocation(140 , 300);
        heightText.setSize(60 , 20);
        heightText.setFont(new Font("Arial", Font.PLAIN, 20));
        add(heightText);

        enemyText = new JTextField();
        enemyText.setLocation(440 , 270);
        enemyText.setSize(60 , 20);
        enemyText.setFont(new Font("Arial", Font.PLAIN, 20));
        add(enemyText);

        start = new JButton();
        start.setText("NEW GAME");
        start.setLocation(220 , 80);
        start.setSize(150 , 50);
        start.setFont(new Font("Arial", Font.BOLD, 15));
        add(start);
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(widthText.getText() != null && heightText.getText()!=null) {
                    int userWidth = parseInt(widthText.getText());
                    int userHeight = parseInt(heightText.getText());
                    int enemyNumber ;
                    try {
                        enemyNumber = parseInt(enemyText.getText());
                    }catch (java.lang.NumberFormatException ne){
                        enemyNumber = -1;
                    }

                    setVisible(false);
                    new GameFrame(userWidth , userHeight , enemyNumber );
                }
            }
        });

        load = new JButton();
        load.setText("LOAD GAME");
        load.setLocation(220 , 140);
        load.setSize(150 , 50);
        load.setFont(new Font("Arial", Font.BOLD, 15));
        load.addActionListener(e -> {
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setDialogTitle("Choose saved file location");
            if (jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                try {
                    ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(new File(jFileChooser.getSelectedFile().toString())));
                    GameFrame g = new GameFrame((Integer) inputStream.readObject(), (Integer) inputStream.readObject() , (Integer)inputStream.readObject());
                    g.rectanglespanel.completelyLoaded = false;
                    g.rectanglespanel.level = (Integer) inputStream.readObject();
                    g.lastGameTime = (long) inputStream.readObject();
                    g.loading = true;
                    g.rectanglespanel.myBomberMan.isAlive = false;
                    g.rectanglespanel.myBomberMan = (BomberMan) inputStream.readObject();
                    g.rectanglespanel.myBomberMan.zero = System.currentTimeMillis();
                    g.rectanglespanel.myBomberMan.lastGameTime = g.lastGameTime;
                    g.loading = false;
                    g.rectanglespanel.myBomberMan.moveAndLevelUp(g.rectanglespanel);
                    g.rectanglespanel.myBomberMan.setScoreControl();
                    g.rectanglespanel.bombs = (ArrayList<Bomb>) inputStream.readObject();
                    g.rectanglespanel.enemies = (ArrayList<Enemy>) inputStream.readObject();
                    for (int i = 0; i < g.rectanglespanel.enemies.size(); i++) {
                        g.rectanglespanel.enemies.get(i).enemyMove(g.rectanglespanel);
                    }
                    g.rectanglespanel.rectangles = new Rectangle[g.rectanglespanel.w][g.rectanglespanel.h];
                    g.rectanglespanel.rectangles = (Rectangle[][]) inputStream.readObject();
                    g.rectanglespanel.thisDoor = (nextLevelDoor) inputStream.readObject();
                    inputStream.close();

                    g.rectanglespanel.completelyLoaded = true;
                } catch (IOException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
                setVisible(false);
            }
        });

        add(load);

    }


}