import javax.swing.*;
import java.io.File;
import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

public abstract class Enemy implements Serializable{
    ImageIcon image;
    int location_i;
    int location_j;
    int pixelI;
    int pixelJ;
    boolean isAlive;
    int ghadam;
    boolean oneIsBAD ;
    boolean twoIsBAD ;
    boolean threeIsBAD ;
    boolean fourIsBAD ;
    public int level;
    int speed ;
    boolean finished ;


    public Enemy(int location_i, int location_j , int i) {
        level = i;
        this.location_i = location_i;
        this.location_j = location_j;
        pixelI = location_i * 50 + 5;
        pixelJ = location_j * 50 + 5;
        isAlive = true;
        ghadam = 10;
        oneIsBAD = false;
        twoIsBAD = false;
        threeIsBAD = false;
        fourIsBAD = false;
        finished = false;
    }


    public void die() {

        this.isAlive = false;
        this.image = new ImageIcon(new File("").getAbsolutePath() + "/images/skull-and-bones.png");

    }

    abstract void enemyMove(RectanglesPanel r);

    void simpleMove(RectanglesPanel r , int i) {
        if (r.myBomberMan.isAlive && this.isAlive  && this!=null) {
            try {
                for (int j = 0; j < 5; j++) {

                    if (i == 1) {
                        if (this.pixelI + 40 < r.w * 50 - this.ghadam) {

                            if (this.location_i < r.w - 1) {

                                if (r.rectangles[(this.pixelI + this.ghadam + 40) / 50][this.location_j].canCross && r.rectangles[(this.pixelI + this.ghadam) / 50][this.location_j].canCross && r.rectangles[(this.pixelI + this.ghadam) / 50][(this.pixelJ + 40) / 50].canCross
                                        && r.rectangles[(this.pixelI + this.ghadam + 40) / 50][(this.pixelJ + 40) / 50].canCross) {
                                    this.right(r);
                                    if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                        r.myBomberMan.die();
                                        break;
                                    }
                                } else {
                                    oneIsBAD = true;
                                    break;
                                }

                            } else {

                                this.right(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }

                            }

                        } else {
                            oneIsBAD = true;
                            break;
                        }

                    }

                    if (i == 2) {

                        if (this.pixelJ >= this.ghadam) {
                            if (this.location_j > 0) {
                                if (r.rectangles[this.location_i][(this.pixelJ - this.ghadam) / 50].canCross && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ - this.ghadam) / 50].canCross) {
                                    this.up(r);
                                    if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                        r.myBomberMan.die();
                                        break;
                                    }
                                } else {
                                    twoIsBAD = true;
                                    break;
                                }


                            } else {
                                this.up(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }
                            }

                        } else {
                            twoIsBAD = true;
                            break;
                        }

                    }

                    if (i == 3) {
                        if (this.pixelI >= this.ghadam) {
                            if (this.location_i > 0) {
                                if (r.rectangles[(this.pixelI - this.ghadam) / 50][(this.pixelJ + 40) / 50].canCross && r.rectangles[(this.pixelI - this.ghadam) / 50][this.location_j].canCross) {
                                    this.left(r);
                                    if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                        r.myBomberMan.die();
                                        break;
                                    }
                                } else {
                                    threeIsBAD = true;
                                    break;
                                }

                            } else {
                                this.left(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }
                            }
                        } else {
                            threeIsBAD = true;
                            break;
                        }

                    }

                    if (i == 4) {

                        if (this.pixelJ + 40 < r.h * 50 - this.ghadam) {

                            if (this.location_j < r.h - 1) {

                                if (r.rectangles[this.location_i][(this.pixelJ + this.ghadam + 40) / 50].canCross && r.rectangles[this.location_i][(this.pixelJ + this.ghadam) / 50].canCross && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam) / 50].canCross
                                        && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam + 40) / 50].canCross) {

                                    this.down(r);
                                    if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                        r.myBomberMan.die();
                                        break;
                                    }

                                } else {
                                    fourIsBAD = true;
                                    break;
                                }

                            } else {

                                this.down(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }
                            }

                        } else {
                            fourIsBAD = true;
                            break;
                        }

                    }

                    try {
                        sleep(200 / speed);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }


                }
            }catch (NullPointerException e){

            }

        }
    }


    void right(RectanglesPanel rec) {
        pixelI += ghadam;
        location_i = pixelI / 50;
        oneIsBAD = false;
        twoIsBAD = false;
        threeIsBAD = false;
        fourIsBAD = false;
    }

    void up(RectanglesPanel rec){

        pixelJ -= ghadam;
        location_j = pixelJ / 50;
        oneIsBAD = false;
        twoIsBAD = false;
        threeIsBAD = false;
        fourIsBAD = false;

    }

    void left(RectanglesPanel rec) {

        pixelI -= ghadam;
        location_i = pixelI / 50;
        oneIsBAD = false;
        twoIsBAD = false;
        threeIsBAD = false;
        fourIsBAD = false;

    }

    void down(RectanglesPanel rec) {
        pixelJ += ghadam;
        location_j = pixelJ / 50;
        oneIsBAD = false;
        twoIsBAD = false;
        threeIsBAD = false;
        fourIsBAD = false;

    }

    void chasingMove(RectanglesPanel r) {

        Thread moveE = new Thread(() -> {
            while (r.myBomberMan.isAlive && this.isAlive &&!finished) {

                if (r.myBomberMan.location_i >= location_i) {
                    threeIsBAD = true;
                }
                if (r.myBomberMan.location_i <= location_i) {
                    oneIsBAD = true;
                }
                if (r.myBomberMan.location_j >= location_j) {
                    twoIsBAD = true;
                }
                if (r.myBomberMan.location_j <= location_j) {
                    fourIsBAD = true;
                }

                int i = 0;

                if (!oneIsBAD || !twoIsBAD || !threeIsBAD || !fourIsBAD) {

                    while ((i == 1 && oneIsBAD) || (i == 2 && twoIsBAD) || (i == 3 && threeIsBAD) || (i == 4 && fourIsBAD) || (i == 0)) {
                        i = ThreadLocalRandom.current().nextInt(1, 5);

                    }

                } else {

                    i = ThreadLocalRandom.current().nextInt(1, 5);

                }

                simpleMove(r, i);
            }

        });
        moveE.start();

    }

}
