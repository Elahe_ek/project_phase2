import javax.swing.*;
import java.io.File;
import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

public class Enemy1 extends Enemy implements Serializable {


    public Enemy1(int location_i, int location_j ) {
        super(location_i, location_j , 1);
        this.image = new ImageIcon(new File("").getAbsolutePath() + "/images/monster.png");
        speed = 1;
    }

    @Override
    void enemyMove(RectanglesPanel r) {
        Thread moveE = new Thread(() -> {

            while (r.myBomberMan.isAlive && this.isAlive && this!=null) {

                int i = 0;

                while ((i == 1 && oneIsBAD) || (i == 2 && twoIsBAD) || (i == 3 && threeIsBAD) || (i == 4 && fourIsBAD) || (i == 0)) {

                    i = ThreadLocalRandom.current().nextInt(1, 5);

                }

                if(this!=null){
                    simpleMove(r , i);
                }else {
                    System.out.println("returned");
                    return;
                }

            }

        });

        moveE.start();
    }

}
