import javax.swing.*;
import java.io.File;
import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

public class Enemy4 extends Enemy implements Serializable{
    public Enemy4(int location_i, int location_j) {
        super(location_i, location_j, 4);
        speed = 2;
        this.image = new ImageIcon(new File("").getAbsolutePath() + "/images/monster-4.png");
    }

    @Override
    void enemyMove(RectanglesPanel r) {

        Thread moveE = new Thread(()->{
            while(r.myBomberMan.isAlive && this.isAlive ){

                if(r.myBomberMan.location_i >= location_i){
                    threeIsBAD= true;
                }
                if(r.myBomberMan.location_i <= location_i){
                    oneIsBAD= true;
                }
                if(r.myBomberMan.location_j >= location_j){
                    twoIsBAD= true;
                }
                if(r.myBomberMan.location_j <= location_j){
                    fourIsBAD= true;
                }

                int i = 0;

                if(!oneIsBAD || !twoIsBAD || !threeIsBAD || !fourIsBAD) {

                    while ((i == 1 && oneIsBAD) || (i == 2 && twoIsBAD) || (i == 3 && threeIsBAD) || (i == 4 && fourIsBAD) || (i == 0)) {
                        i = ThreadLocalRandom.current().nextInt(1, 5);

                    }

                }
                else {

                    i = ThreadLocalRandom.current().nextInt(1, 5);

                }

                Move(r , i);

                if(finished){
                    return;
                }
            }

        });
        moveE.start();

    }

    void Move(RectanglesPanel r , int i) {
        if (r.myBomberMan.isAlive && this.isAlive && !finished) {
            for (int j = 0; j < 5; j++) {

                if (i == 1) {
                    if (this.pixelI + 40 < r.w * 50 - this.ghadam) {

                        if (this.location_i < r.w - 1) {

                            if (r.noBomb(r.rectangles[(this.pixelI + this.ghadam + 40) / 50][this.location_j]) && r.noBomb(r.rectangles[(this.pixelI + this.ghadam) / 50][this.location_j])
                                    && r.noBomb(r.rectangles[(this.pixelI + this.ghadam) / 50][(this.pixelJ + 40) / 50])
                                    && r.noBomb(r.rectangles[(this.pixelI + this.ghadam + 40) / 50][(this.pixelJ + 40) / 50])) {
                                this.right(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }
                            } else {
                                oneIsBAD = true;
                                break;
                            }

                        } else {

                            this.right(r);
                            if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                r.myBomberMan.die();
                                break;
                            }

                        }

                    } else {
                        oneIsBAD = true;
                        break;
                    }

                }

                if (i == 2) {

                    if (this.pixelJ >= this.ghadam) {
                        if (this.location_j > 0) {
                            if (r.noBomb(r.rectangles[this.location_i][(this.pixelJ - this.ghadam) / 50]) && r.noBomb(r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ - this.ghadam) / 50])) {
                                this.up(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }
                            } else {
                                twoIsBAD = true;
                                break;
                            }


                        } else {
                            this.up(r);
                            if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                r.myBomberMan.die();
                                break;
                            }
                        }

                    } else {
                        twoIsBAD = true;
                        break;
                    }

                }

                if (i == 3) {
                    if (this.pixelI >= this.ghadam) {
                        if (this.location_i > 0) {
                            if (r.noBomb(r.rectangles[(this.pixelI - this.ghadam) / 50][(this.pixelJ + 40) / 50]) && r.noBomb(r.rectangles[(this.pixelI - this.ghadam) / 50][this.location_j])) {
                                this.left(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }
                            } else {
                                threeIsBAD = true;
                                break;
                            }

                        } else {
                            this.left(r);
                            if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                r.myBomberMan.die();
                                break;
                            }
                        }
                    } else {
                        threeIsBAD = true;
                        break;
                    }

                }

                if (i == 4) {

                    if (this.pixelJ + 40 < r.h * 50 - this.ghadam) {

                        if (this.location_j < r.h - 1) {

                            if (r.noBomb(r.rectangles[this.location_i][(this.pixelJ + this.ghadam + 40) / 50]) && r.noBomb(r.rectangles[this.location_i][(this.pixelJ + this.ghadam) / 50]) &&
                                    r.noBomb(r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam) / 50]) && r.noBomb(r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam + 40) / 50])) {

                                this.down(r);
                                if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                    r.myBomberMan.die();
                                    break;
                                }

                            } else {
                                fourIsBAD = true;
                                break;
                            }

                        } else {

                            this.down(r);
                            if (this.location_i == r.myBomberMan.location_i && this.location_j == r.myBomberMan.location_j) {
                                r.myBomberMan.die();
                                break;
                            }
                        }

                    } else {
                        fourIsBAD = true;
                        break;
                    }

                }

                try {
                    sleep(200 / speed);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }


            }
        }
    }

}
