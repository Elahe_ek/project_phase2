import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.Serializable;

public class Bonus implements Serializable{

    boolean isItGood ;
    int type;
    ImageIcon image;
    Boolean released;


    public Bonus(boolean good , int t ) {
        this.isItGood = good;
        this.type = t;

        if(t==1){
            if(good)
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/goodspeed.png");
            else{
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/badspeed.png");
            }
        }
        if(t==2){
            if(good)
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/goodscore.png");
            else{
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/badscore.png");
            }
        }
        if(t==3){
            if(good)
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/goodlimit.png");
            else{
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/badlimit.png");
            }
        }
        if(t==4){
            if(good)
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/goodradius.png");
            else{
                this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/badradius.png");
            }
        }

        if(t==5){
            this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/ghost.png");
        }

        if(t==6){
            this.image =  new ImageIcon(new File("").getAbsolutePath()+"/images/control.png");
        }

        released = false;
    }

}
