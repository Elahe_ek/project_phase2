import javax.swing.*;
import java.io.File;
import java.io.Serializable;

import static java.lang.Thread.sleep;

public class BomberMan implements Serializable{

    int bombLimit ;
    int bombRadius;
    ImageIcon image;
    int location_i ;
    int location_j;
    boolean isAlive;
    int pixelI;
    int pixelJ;
    int ghadam;
    int score;
    int speed ;
    long zero ;
    boolean ghostMode ;
    boolean bombControling;
    boolean finished;
    long lastGameTime;

    BomberMan() {
        bombLimit = 1;
        bombRadius = 2;
        location_i = 0;
        location_j = 0;
        pixelI =5;
        pixelJ =5;
        speed = 2 ;
        ghadam = speed*5 ;
        isAlive = true;
        score =0 ;
        ghostMode = false;
        bombControling = false;
        finished = false;
        image = new ImageIcon(new File("").getAbsolutePath()+"/images/superhero.png");
        zero = System.currentTimeMillis();
        lastGameTime = 0;
    }

    public void setScoreControl(){
        Thread scoreControl = new Thread(()->{
            while(isAlive) {
                if (System.currentTimeMillis() - zero +  lastGameTime> 5 * 60 * 1000)
                    score -= 1;

                if (score < 0)
                    die();

                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        scoreControl.start();
    }

    public void die() {
        this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/dead.png");
        isAlive= false;
        JOptionPane.showMessageDialog(null, "Game Over");
    }

    public void moveAndLevelUp(RectanglesPanel r) {

        Thread bomberManMovingThread = new Thread(() -> {

            while (isAlive) {

                if (r.move == 1) {
                    if (this.pixelI < r.w * 50 - 40 - this.ghadam) {

                        if (this.location_i < r.w - 1) {

                            if (r.rectangles[(this.pixelI + this.ghadam + 40) / 50][this.location_j].canCrossBomberman && r.rectangles[(this.pixelI + this.ghadam) / 50][this.location_j].canCrossBomberman && r.rectangles[(this.pixelI + this.ghadam) / 50][(this.pixelJ + 40) / 50].canCrossBomberman
                                    && r.rectangles[(this.pixelI + this.ghadam + 40) / 50][(this.pixelJ + 40) / 50].canCrossBomberman) {
                                this.right(r);
                            }

                            if (r.rectangles[(this.pixelI + this.ghadam) / 50][this.location_j] instanceof Grass && (this.pixelI + this.ghadam) / 50 == this.location_i) {

                                if (((Grass) r.rectangles[(this.pixelI + this.ghadam) / 50][this.location_j]).bomb != null) {

                                    if ((this.pixelJ + 40) / 50 != this.location_j) {
                                        if (r.rectangles[(this.pixelI + this.ghadam) / 50][(this.pixelJ + 40) / 50].canCrossBomberman) {
                                            this.right(r);
                                        }
                                    }
                                    this.right(r);
                                }
                            }


                        } else {

                            this.right(r);
                        }

                    }

                    r.move = 0;
                }

                if (r.move == 2) {

                    if (this.pixelJ >= this.ghadam) {
                        if (this.location_j > 0) {
                            if (r.rectangles[this.location_i][(this.pixelJ - this.ghadam) / 50].canCrossBomberman && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ - this.ghadam) / 50].canCrossBomberman) {
                                this.up(r);
                            }
                            if (r.rectangles[this.location_i][(this.pixelJ - this.ghadam) / 50] instanceof Grass && (this.pixelJ - this.ghadam) == this.location_j) {
                                if (((Grass) r.rectangles[this.location_i][(this.pixelJ - this.ghadam) / 50]).bomb != null) {

                                    if (r.rectangles[this.location_i][(this.pixelJ - this.ghadam + 40) / 50].canCrossBomberman && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ - this.ghadam) / 50].canCrossBomberman
                                            && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ - this.ghadam + 40) / 50].canCrossBomberman) {
                                        this.up(r);
                                    }
                                }

                            }


                        } else {
                            this.up(r);
                        }

                    }
                    r.move = 0;
                }

                if (r.move == 3) {
                    if (this.pixelI >= this.ghadam) {
                        if (this.location_i > 0) {
                            if (r.rectangles[(this.pixelI - this.ghadam) / 50][(this.pixelJ + 40) / 50].canCrossBomberman && r.rectangles[(this.pixelI - this.ghadam) / 50][this.location_j].canCrossBomberman) {
                                this.left(r);
                            }
                            if (r.rectangles[(this.pixelI - this.ghadam) / 50][this.location_j] instanceof Grass && (this.pixelI - this.ghadam) / 50 == this.location_i) {
                                if (((Grass) r.rectangles[(this.pixelI - this.ghadam) / 50][this.location_j]).bomb != null) {
                                    if ((this.pixelJ + 40) / 50 != this.location_j) {
                                        if (r.rectangles[(this.pixelI - this.ghadam) / 50][(this.pixelJ + 40) / 50].canCrossBomberman) {
                                            this.left(r);
                                        }
                                    } else {
                                        this.left(r);
                                    }
                                }
                            }

                        } else {
                            this.left(r);
                        }
                    }
                    r.move = 0;
                }

                if (r.move == 4) {

                    if (this.pixelJ < r.h * 50 - 40 - this.ghadam) {

                        if (this.location_j < r.h - 1) {

                            if (r.rectangles[this.location_i][(this.pixelJ + this.ghadam + 40) / 50].canCrossBomberman && r.rectangles[this.location_i][(this.pixelJ + this.ghadam) / 50].canCrossBomberman && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam) / 50].canCrossBomberman
                                    && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam + 40) / 50].canCrossBomberman) {

                                this.down(r);

                            }

                            if (r.rectangles[this.location_i][(this.pixelJ + this.ghadam) / 50] instanceof Grass && (this.pixelJ + this.ghadam)/50 == this.location_j) {

                                if (((Grass) r.rectangles[this.location_i][location_j]).bomb != null) {

                                    if (r.rectangles[this.location_i][(this.pixelJ + this.ghadam + 40) / 50].canCrossBomberman
                                            && r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam + 40) / 50].canCrossBomberman) {
                                        if(!((this.pixelI+40/50)==location_i && (this.pixelJ+this.ghadam)/50 == location_j)){
                                            if(r.rectangles[(this.pixelI + 40) / 50][(this.pixelJ + this.ghadam) / 50].canCrossBomberman);
                                            this.down(r);
                                        }
                                        else{
                                            this.down(r);
                                        }

                                    }
                                }

                            }

                        } else {

                            this.down(r);

                        }

                    }

                    r.move = 0;
                }

                try {
                    sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if ( location_i == r.thisDoor.i && location_j == r.thisDoor.j && r.allEnemiesDied()){

                    r.levelUp();

                }

            }

        });
        bomberManMovingThread.start();
    }

    public void up(RectanglesPanel r) {

        pixelJ-= ghadam;
        location_j= pixelJ/50;
        checkBonus(r);
    }

    public void down(RectanglesPanel r ) {

        pixelJ += ghadam;
        location_j= pixelJ/50;
        checkBonus(r);
    }

    public void right(RectanglesPanel r) {

        pixelI += ghadam;
        location_i= pixelI/50;
        checkBonus(r);
    }

    public void left(RectanglesPanel r) {

        pixelI -= ghadam;
        location_i= pixelI/50;
        checkBonus(r);

    }

    private void checkBonus(RectanglesPanel r ) {
        if( r.rectangles[location_i][location_j] instanceof Grass) {
            if (((Grass) (r.rectangles[location_i][location_j])).bonus != null)
                eatBonus(r, ((Grass) (r.rectangles[location_i][location_j])).bonus);
        }
    }

    private void eatBonus(RectanglesPanel r, Bonus bonus) {

        if(bonus.type==1){
            if(bonus.isItGood)
                speed+=1;
            else if (speed >1 )
                speed-=1;
        }

        if(bonus.type==2){
            if(bonus.isItGood)
                score+=100;
            else
                score-=100;
        }

        if(bonus.type==3){
            if(bonus.isItGood)
                bombLimit+=1;
            else if(bombLimit > 1)
                bombLimit-=1;
        }

        if(bonus.type==4){

            if(bonus.isItGood)
                bombRadius+=1;
            else if( bombRadius > 1)
                bombRadius-=1;
        }

        if(bonus.type==5){
            for (int i = 0; i <r.w ; i++) {
                for (int j = 0; j < r.h; j++) {
                    if(i%2!=0 && j%2!=0)
                        r.rectangles[i][j].canCrossBomberman = true;
                }
            }
            ghostMode = true;
        }

        if(bonus.type==6){
            bombControling = true;
            System.out.println(bombControling);
        }

        ((Grass)(r.rectangles[location_i][location_j])).bonus=null;
    }

}
