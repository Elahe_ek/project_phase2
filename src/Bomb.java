import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

public class Bomb implements Serializable{
    ImageIcon image;
    int i;
    int j;
    boolean jPlusContinue = true;
    boolean jMinusContinue = true;
    boolean iPlusContinue= true;
    boolean iMinusContinue= true;

    Bomb(int i, int j) {
        this.i = i;
        this.j = j;
        this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/b.png");
    }

    public void explode(BomberMan bomberMan) {
        
        Thread exploding = new Thread(() -> {

            try {
                Thread.sleep(4800);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            if(bomberMan.isAlive)
                this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/explosion.png");
        });
        exploding.start();
        
    }

    public void quickExplode(BomberMan bomberMan) {

        Thread nowExploding = new Thread(() -> {

            if(bomberMan.isAlive)
                this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/explosion.png");
        });
        nowExploding.start();

    }

    public void quickRemove(RectanglesPanel r) {

        Thread qRemovingBomb = new Thread(() -> {

            try {
                sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            if(r.myBomberMan.isAlive) {
                ((Grass) r.rectangles[this.i][this.j]).bomb = null;
                r.rectangles[this.i][this.j].canCross = true;
                r.rectangles[this.i][this.j].canCrossBomberman = true;
                r.bombs.remove(this);
                destroyNeighbors(r);
            }

        });

        qRemovingBomb.start();
    }

    public void remove(RectanglesPanel r) {

        Thread removingBomb = new Thread(() -> {

            try {
                sleep(5000);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            if(r.myBomberMan.isAlive) {
                ((Grass) r.rectangles[this.i][this.j]).bomb = null;
                r.rectangles[this.i][this.j].canCross = true;
                r.rectangles[this.i][this.j].canCrossBomberman =  true;
                r.bombs.remove(this);
                destroyNeighbors(r);
            }

        });

        removingBomb.start();
    }

    public void destroyNeighbors(RectanglesPanel rP) {

        if (this.i == rP.myBomberMan.location_i && this.j == rP.myBomberMan.location_j) {

            rP.myBomberMan.die();

        }

        for (int i = 1; i <= rP.myBomberMan.bombRadius; i++) {

            if (rP.myBomberMan.isAlive) {

                int thisI = i;
                Thread exploding = new Thread(() -> {

                    try {
                        sleep(thisI * 200);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }

                    try {
                        if (this.jPlusContinue) {

                            if (rP.rectangles[this.i][this.j + thisI] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i][this.j + thisI]).destroy();
                                rP.removeWall(this.i, this.j + thisI);
                                this.jPlusContinue = false;
                            }

                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if (rP.enemies.get(j).location_i == this.i && rP.enemies.get(j).location_j == this.j + thisI) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j);
                                    }
                                }
                            }

                            if (rP.rectangles[this.i][this.j + thisI] instanceof Rock)
                                this.jPlusContinue = false;
                            if (this.i == rP.myBomberMan.location_i && this.j + thisI == rP.myBomberMan.location_j) {
                                rP.myBomberMan.die();
                            }

                        }

                        if (this.jMinusContinue) {

                            if (rP.rectangles[this.i][this.j - thisI] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i][this.j - thisI]).destroy();
                                rP.removeWall(this.i, this.j - thisI);
                                this.jMinusContinue = false;
                            }

                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if (rP.enemies.get(j).location_i == this.i && rP.enemies.get(j).location_j == this.j - thisI) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j);
                                    }
                                }
                            }

                            if (rP.rectangles[this.i][this.j - thisI] instanceof Rock)
                                this.jMinusContinue = false;

                            if (this.i == rP.myBomberMan.location_i && this.j - thisI == rP.myBomberMan.location_j) {
                                rP.myBomberMan.die();
                            }

                        }



                        if (this.iPlusContinue) {
                            if (rP.rectangles[this.i + thisI][this.j] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i + thisI][this.j]).destroy();
                                rP.removeWall(this.i + thisI, this.j);
                                this.iPlusContinue = false;
                            }

                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if ( rP.enemies.get(j).location_i == this.i + thisI && rP.enemies.get(j).location_j == this.j) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j);
                                    }
                                }

                            }

                            if (rP.rectangles[this.i + thisI][this.j] instanceof Rock)
                                this.iPlusContinue = false;

                            if (this.i + thisI == rP.myBomberMan.location_i && this.j == rP.myBomberMan.location_j) {
                                rP.myBomberMan.die();
                            }

                        }

                        if (this.iMinusContinue) {
                            if (rP.rectangles[this.i - thisI][this.j] instanceof Wall) {
                                ((Wall) rP.rectangles[this.i - thisI][this.j]).destroy();
                                rP.removeWall(this.i - thisI, this.j);
                                this.iMinusContinue = false;
                            }
                            for (int j = 0; j < rP.enemies.size(); j++) {
                                if (rP.enemies.get(j) != null) {
                                    if (rP.enemies.get(j).location_i == this.i - thisI && (int) rP.enemies.get(j).location_j == this.j) {
                                        rP.enemies.get(j).die();
                                        rP.removeEnemy(j);
                                    }
                                }
                            }
                            if (rP.rectangles[this.i - thisI][this.j] instanceof Rock)
                                this.iMinusContinue = false;
                            if (this.i - thisI == rP.myBomberMan.location_i && this.j == rP.myBomberMan.location_j) {
                                rP.myBomberMan.die();
                            }
                        }
                    } catch (Exception e) {

                    }

                });
                exploding.start();

            }
        }


    }
}