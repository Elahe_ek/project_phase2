import javax.swing.*;
import java.io.File;
import java.io.Serializable;

public class Wall extends Rectangle implements Serializable{
    Bonus bonus;
    boolean destroyed ;

    Wall(int x, int y, int width, int height) {
        super(x, y, width, height , false, new ImageIcon(new File("").getAbsolutePath()+"/images/gate.png"));
        bonus = null;
        destroyed = false;
    }


    public void destroy() {

        this.image = new ImageIcon(new File("").getAbsolutePath()+"/images/explosion-2.png");

    }

}
