import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.*;
import java.util.ArrayList;
import java.util.concurrent.ThreadLocalRandom;

import static java.lang.Thread.sleep;

class RectanglesPanel extends JPanel implements KeyListener,ActionListener, Serializable {
    Timer timer = new Timer(100, this);
    private static final int PREF_W = 50;
    private static final int PREF_H = PREF_W;
    Rectangle[][] rectangles ;
    boolean completelyLoaded = true;
    BomberMan myBomberMan;
    GameFrame gameFrame ;
    int w ;
    int h ;
    int level ;
    int enemyNum ;
    int translateX ;
    int translateY ;
    int move = 0;
    nextLevelDoor thisDoor;
    ArrayList<Integer>randomX = new ArrayList<>();
    ArrayList<Integer> randomY = new ArrayList<>();
    ArrayList<Bomb> bombs = new ArrayList<>();
    ArrayList<Enemy> enemies = new ArrayList<>();
    ArrayList<Wall> randomWalls = new ArrayList<>();
    ArrayList<Integer> wallsForBonus = new ArrayList<>();

    RectanglesPanel(int width, int height , GameFrame g , BomberMan my, int l , int enemyInput) {
        this.w = width;
        this.h=height;
        this.gameFrame = g;
        this.level = l;
        translateX =0;
        translateY = 0;

        if(enemyInput == -1)
            enemyNum = Math.min(w/ 3, h/3);
        else
            enemyNum = enemyInput;

        myBomberMan = my;
        addKeyListener(this);
        setFocusable(true);
        timer.start();
        makeRandomWalls();
        addRectangle();
        makeEnemies(level , enemyNum);
        makeRandomBonus();
        makeNextLevelDoor();
        for (int i = 0; i <enemies.size() ; i++) {
            enemies.get(i).enemyMove( this  );
        }
        myBomberMan.moveAndLevelUp(this);
        myBomberMan.setScoreControl();

    }

    public void makeRandomWalls() {
        for (int i = 0; i <((w*h) /5) ; i++) {
            randomX.add(0) ;
            randomY.add(0);
            while(!canBeWall(randomX.get(i) , randomY.get(i))){
                randomX.set(i ,ThreadLocalRandom.current().nextInt(0, w) ) ;
                randomY.set(i , ThreadLocalRandom.current().nextInt(0, h)) ;
            }
        }
    }

    public boolean canBeWall(int thisX , int thisY) {
        if(thisX % 2 == 1 && thisY % 2 ==1)
            return false;

        return (!((thisX==0 || thisX==1)&&(thisY==0 || thisY==1))) ;
    }

    public boolean isRandomWall(int this_i, int this_j) {

        for (int i = 0; i < ((w*h)/5) ; i++) {
            if(randomX.size()>i && randomY.size()>i) {
                if (randomX.get(i) == this_i && randomY.get(i) == this_j)
                    return true;
            }
        }
        return false;
    }

    public void addRectangle() {
        rectangles = new Rectangle[w][h];
        for (int i = 0; i < w; i++) {
            for (int j = 0; j < h; j++) {
                boolean rock = false;
                Rectangle rect = null;
                if (i % 2 != 0 && j % 2 != 0) {
                    rect = new Rock(i * 50, j * 50, 50, 50);
                    rock = true;
                }
                if (isRandomWall(i, j)) {
                    rect = new Wall(i * 50, j * 50, 50, 50);
                    randomWalls.add((Wall) rect);
                }

                if(!rock && !isRandomWall(i , j)){
                    rect = new Grass(i * 50, j * 50, 50, 50);
                }
                rectangles[i][j] = rect;
            }
        }
    }

    private Enemy addEnemy(int t , int enemiesMade){
        if(t==1) {
            Enemy thisEnemy = new Enemy1(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (!canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy1(i1, j1);
        }
        if(t==2) {
            Enemy thisEnemy = new Enemy2(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (!canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy2(i1, j1);
        }
        if(t==3) {
            Enemy thisEnemy = new Enemy3(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy3(i1, j1);
        }
        if(t==4) {
            Enemy thisEnemy = new Enemy4(0, 0);
            enemies.add(thisEnemy);
            int i1 = 0;
            int j1 = 0;
            while (!canBeEnemy(i1, j1) || hasBeenMadeEnemy(i1, j1, enemiesMade)) {
                i1 = ThreadLocalRandom.current().nextInt(0, w);
                j1 = ThreadLocalRandom.current().nextInt(0, h);
            }

            return new Enemy4(i1, j1);
        }

        return null;

    }

    private void makeEnemies(int level , int num) {

        int enemiesMade = 0 ;

        if(level ==1 ) {
            for (int i = 0; i < num ; i++) {
                Enemy e = addEnemy(1 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
        }
        if(level==2){
            for (int i = 0; i < num; i++) {
                int type = ThreadLocalRandom.current().nextInt(1, 3);
                if(type==1){
                    Enemy e = addEnemy(1 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
                else {
                    Enemy e = addEnemy(2 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
            }
        }

        if(level ==3 ){
            for (int i = 0; i < num; i++) {
                int type = ThreadLocalRandom.current().nextInt(1, 4);
                if(type==1){
                    Enemy e = addEnemy(1 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
                if(type==2) {
                    Enemy e = addEnemy(2 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }
                if(type==3){
                    Enemy e = addEnemy(3 , enemiesMade);
                    enemies.set(i, e);
                    enemiesMade++;
                }

            }
        }

        if(level ==4 ){
            ifLevelFourReached(num, enemiesMade);
        }

        if(level > 4 ){
            ifLevelFourReached((num*105)/100, enemiesMade);
        }

    }

    public void ifLevelFourReached(int number , int enemiesMade){

        for (int i = 0; i < number ; i++) {
            int type = ThreadLocalRandom.current().nextInt(1, 5);
            if(type==1){
                Enemy e = addEnemy(1 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
            if(type==2) {
                Enemy e = addEnemy(2 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
            if(type==3){
                Enemy e = addEnemy(3 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }
            if(type==4){
                Enemy e = addEnemy(4 , enemiesMade);
                enemies.set(i, e);
                enemiesMade++;
            }

        }
    }

    private boolean canBeEnemy(int i , int j) {

        if(i % 2 == 1 && i % 2 ==1)
            return false;

        if(isRandomWall(i , j))
            return false;

        return (!((i==0 || i==1)&&(j==0 || j==1))) ;

    }

    private boolean hasBeenMadeEnemy(int i1 , int j1 , int enemiesMade){
        for (int j = 0; j < enemiesMade; j++) {
            if (enemies.get(j).location_i==i1 && enemies.get(j).location_j==j1){
                return  true;
            }

        }
        return false;
    }

    public void makeRandomBonus() {
        for (int i = 0; i < 2 * enemies.size(); i++) {
            int whichWall = 0;
            try {
                while (randomWalls.get(whichWall).bonus != null || hasBeenGivenBonus(whichWall)) {
                    whichWall = ThreadLocalRandom.current().nextInt(0, randomX.size());
                }
            }catch(IndexOutOfBoundsException e){

            }
            int goodorBAD = ThreadLocalRandom.current().nextInt(0, 2);
            boolean good = true;
            if (goodorBAD == 1)
                good = false;

            int whichType = ThreadLocalRandom.current().nextInt(1, 7);
            try{
                ((Wall) (rectangles[randomX.get(whichWall)][randomY.get(whichWall)])).bonus = new Bonus(good, whichType);
            }catch(java.lang.ClassCastException cast){

            }
            wallsForBonus.add(whichWall);

        }
    }

    private boolean hasBeenGivenBonus(int whichWall) {
        for (int i = 0; i < wallsForBonus.size(); i++) {
            if(wallsForBonus.get(i)== whichWall)
                return true;
        }
        return false;
    }

    private void makeNextLevelDoor() {
        int door = ThreadLocalRandom.current().nextInt(0, randomX.size());

        try{
            while(((Wall)rectangles[randomX.get(door)][randomY.get(door)]).bonus!=null ){
                door = ThreadLocalRandom.current().nextInt(0, randomX.size());
            }
        }catch(java.lang.ClassCastException ec){

        }

        thisDoor = new nextLevelDoor(randomX.get(door) , randomY.get(door));
    }

    public Dimension getPreferredSize() {
        return new Dimension(PREF_W, PREF_H);
    }

    public void levelUp() {
        level ++ ;
        for (int i = 0; i <randomX.size() ; i++) {
            randomX.remove(i);
        }
        for (int i = 0; i <randomY.size() ; i++) {
            randomY.remove(i);
        }
        for (int i = 0; i <randomWalls.size() ; i++) {
            randomWalls.remove(i);
        }
        makeRandomWalls();
        addRectangle();
        for (int i = 0; i <enemies.size() ; i++) {
            enemies.get(i).finished = true;
            enemies.remove(i);
        }
        for (int i = 0; i <wallsForBonus.size() ; i++) {
            wallsForBonus.remove(i);
        }
        makeEnemies(level , enemyNum);
        makeRandomBonus();
        makeNextLevelDoor();
        for (int i = 0; i <enemies.size() ; i++) {
            enemies.get(i).enemyMove( this  );
        }
        myBomberMan.pixelI = 5;
        myBomberMan.pixelJ = 5;
        myBomberMan.location_i = 0;
        myBomberMan.location_j = 0;
    }

    public boolean allEnemiesDied() {
        for (int i = 0; i <enemies.size() ; i++) {
            try {
                if (!enemies.get(i).equals(null))
                    return false;
            }catch(NullPointerException ee){

            }
        }
        return true;
    }

    public void removeEnemy(int i) {
        Thread killing = new Thread(() -> {

            try {
                sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            myBomberMan.score+=enemies.get(i).level *20;
            enemies.remove(i);

        });
        killing.start();
    }

    public void removeWall(int i, int j) {

        Thread destroying = new Thread(() -> {

            try {
                sleep(200);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }

            ((Wall)rectangles[i][j]).destroyed=true ;
            Bonus b = ((Wall)rectangles[i][j]).bonus ;
            rectangles[i][j]= new Grass(i , j , 50 , 50);
            ((Grass)rectangles[i][j]).bonus = b ;
            if(b!= null)
                b.released = true;
            myBomberMan.score+=10;

            if(thisDoor.i==i && thisDoor.j==j)
                thisDoor.found = true;

        });
        destroying.start();
        randomX.remove(i);
        randomY.remove(j);

    }

    public boolean noBomb(Rectangle r){
        if(!(r instanceof Grass))
            return true;
        else if(((Grass) r).bomb != null)
            return false ;
        return true;
    }

    public void Save(File file){
        try {
            ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(file));
            outputStream.writeObject(w);
            outputStream.writeObject(h);
            outputStream.writeObject(enemies.size());
            outputStream.writeObject(level);
            outputStream.writeObject(System.currentTimeMillis()-myBomberMan.zero);
            outputStream.writeObject(myBomberMan);
            outputStream.writeObject(bombs);
            outputStream.writeObject(enemies);
            outputStream.writeObject(rectangles);
            outputStream.writeObject(thisDoor);
            outputStream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void Load(File file) {
        completelyLoaded = false;

        try {
            ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(file));
            w = (Integer) inputStream.readObject();
            h = (Integer) inputStream.readObject();
            inputStream.readObject();
            gameFrame.setSize(w*50 , h*50 +60 );
            level = (Integer) inputStream.readObject();
            gameFrame.lastGameTime = (long) inputStream.readObject();
            gameFrame.dataPanel.setSize(w*50 , 20);
            gameFrame.playerPanel.setSize(w*50 , 20);
            gameFrame.loading = true;
            myBomberMan.isAlive = false;
            myBomberMan = (BomberMan)inputStream.readObject() ;
            myBomberMan.zero = System.currentTimeMillis();
            myBomberMan.lastGameTime = gameFrame.lastGameTime;
            gameFrame.loading= false;
            myBomberMan.moveAndLevelUp(this);
            myBomberMan.setScoreControl();
            for (int i = 0; i < enemies.size() ; i++) {
                enemies.get(i).isAlive = false;
                enemies.remove(i);
            }
            bombs = (ArrayList<Bomb>) inputStream.readObject();
            enemies = (ArrayList<Enemy>)inputStream.readObject();
            for (int i = 0; i <enemies.size() ; i++) {
                enemies.get(i).enemyMove( this  );
            }
            rectangles = new Rectangle[w][h];
            rectangles = (Rectangle[][])inputStream.readObject();
            thisDoor = (nextLevelDoor)inputStream.readObject();
            inputStream.close();

            completelyLoaded = true;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        if(completelyLoaded) {

            if(myBomberMan.pixelI - translateX > 14*50 && myBomberMan.pixelI < w*50 - 15*50 +20)
                translateX += myBomberMan.ghadam ;
            if(myBomberMan.pixelI - translateX < 14*50 && myBomberMan.pixelI > 14 * 50 )
                translateX -= myBomberMan.ghadam ;

            if(myBomberMan.pixelJ - translateY > 7*50 && myBomberMan.pixelJ < h*50 - 8*50 +25 )
                translateY += myBomberMan.ghadam ;
            if(myBomberMan.pixelJ - translateY < 7*50 && myBomberMan.pixelJ > 7 * 50 )
                translateY -= myBomberMan.ghadam ;

            for (int k = 0; k < w; k++) {
                for (int l = 0; l < h; l++) {
                    g2.drawImage(rectangles[k][l].grassImage.getImage(), k * 50- translateX, l * 50 - translateY, null);

                    g.drawImage(rectangles[k][l].image.getImage(), k * 50 - translateX, l * 50 - translateY, null);
                    if(rectangles[k][l] instanceof Grass){
                        if(((Grass)rectangles[k][l]).bomb!= null){
                            g.drawImage(((Grass)rectangles[k][l]).bomb.image.getImage(), k * 50- translateX, l * 50 - translateY, null);
                        }
                        if(((Grass)rectangles[k][l]).bonus!= null && ((Grass)rectangles[k][l]).bonus.released ){
                            g.drawImage(((Grass)rectangles[k][l]).bonus.image.getImage(), k * 50 +5 - translateX, l * 50+5-translateY , null);
                        }

                    }

                }
            }

            for (int i = 0; i <enemies.size() ; i++) {
                if(enemies.get(i)!= null) {
                    g2.drawImage(enemies.get(i).image.getImage(), enemies.get(i).pixelI - translateX, enemies.get(i).pixelJ - translateY, null);
                }


            }

            if(thisDoor!= null && thisDoor.found)
                g2.drawImage(thisDoor.image.getImage() , thisDoor.i*50 - translateX, thisDoor.j*50 - translateY , null);

            g2.drawImage(myBomberMan.image.getImage(), myBomberMan.pixelI - translateX, myBomberMan.pixelJ - translateY , null);

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if (e.getKeyCode() == KeyEvent.VK_SPACE) {

            if(myBomberMan.bombLimit> bombs.size() && myBomberMan.isAlive) {

                Bomb this_bomb = new Bomb(myBomberMan.location_i, myBomberMan.location_j);
                ((Grass) rectangles[myBomberMan.location_i][myBomberMan.location_j]).bomb = this_bomb;
                bombs.add(this_bomb);
                rectangles[myBomberMan.location_i][myBomberMan.location_j].canCross = false;
                rectangles[myBomberMan.location_i][myBomberMan.location_j].canCrossBomberman = false;
                this_bomb.explode(myBomberMan);
                this_bomb.remove(this);


            }
        }

        if (e.getKeyCode() == KeyEvent.VK_RIGHT && myBomberMan.isAlive) {

            move=1;

        }

        if (e.getKeyCode() == KeyEvent.VK_UP && myBomberMan.isAlive) {

            move=2;


        }

        if (e.getKeyCode() == KeyEvent.VK_LEFT && myBomberMan.isAlive) {

            move = 3;

        }

        if (e.getKeyCode() == KeyEvent.VK_DOWN && myBomberMan.isAlive) {

            move =4;

        }

        if (e.getKeyCode() == KeyEvent.VK_ENTER && myBomberMan.isAlive &&myBomberMan.bombControling) {

            try {
                bombs.get(0).quickExplode(myBomberMan);
                bombs.get(0).quickRemove(this);
            }catch (Exception ex){

            }

        }

        if (e.getModifiers() == KeyEvent.CTRL_MASK && e.getKeyCode() == 79) {
            JFileChooser jFileChooser = new JFileChooser();
            if (jFileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
                Load(new File(jFileChooser.getSelectedFile().toString()));

            }
        }

        if (e.getModifiers() == KeyEvent.CTRL_MASK && e.getKeyCode() == 83) {
            JFileChooser jFileChooser = new JFileChooser();
            jFileChooser.setDialogTitle("choose save location");
            if (jFileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
                Save(new File(jFileChooser.getSelectedFile() + ".game"));
            }
        }

    }
    @Override
    public void keyReleased(KeyEvent e) {

    }

}
