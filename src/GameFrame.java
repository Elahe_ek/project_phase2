import javax.swing.*;
import java.awt.*;
import java.io.*;

import static java.lang.Thread.sleep;

public class GameFrame extends JFrame implements Serializable {
    JPanel dataPanel;
    JPanel playerPanel;
    RectanglesPanel rectanglespanel;
    int width ;
    int height ;
    int enemy ;
    long lastGameTime ;
    boolean loading ;

    GameFrame(int w , int h  , int enemies) {
        loading = false;
        width = w ;
        height = h ;
        enemy = enemies ;
        lastGameTime = 0;
        setTitle("BomberMan");
        setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS));
        BomberMan myBomber = new BomberMan();
        rectanglespanel = new RectanglesPanel(w , h , this  , myBomber , 1 , enemy);

        dataPanel = new JPanel();
        dataPanel.setPreferredSize(new Dimension(w*50, 25));
        dataPanel.setMinimumSize(new Dimension(w*50, 25));
        dataPanel.setMaximumSize(new Dimension(w*50, 25));
        dataPanel.setBackground(Color.lightGray);
        dataPanel.setLayout(new BoxLayout(dataPanel , 0));

        JLabel levelLabel = new JLabel("Level "+rectanglespanel.level);
        levelLabel.setSize(w*10 , 20);
        levelLabel.setMinimumSize(new Dimension(100 , 20));
        levelLabel.setMaximumSize(new Dimension(100 , 20));
        Thread level = new Thread(()->{
            while(true) {
                levelLabel.setText("Level "+rectanglespanel.level);
            }
        });
        level.start();
        dataPanel.add(levelLabel);

        JLabel timeLabel = new JLabel("Time : ");
        timeLabel.setSize(w*10 , 20);
        timeLabel.setMinimumSize(new Dimension(100 , 20));
        timeLabel.setMaximumSize(new Dimension(100 , 20));
        Thread time = new Thread(()->{
            while (rectanglespanel.myBomberMan.isAlive || loading ) {
                long t = System.currentTimeMillis()- rectanglespanel.myBomberMan.zero + lastGameTime ;
                long minutes = t / 60000 ;
                long seconds = (t - ((t / 60000) * 60000)) / 1000 ;
                String secondsString = "" +seconds;
                if(seconds<10)
                    secondsString = "0"+seconds;

                timeLabel.setText("Time : " + minutes + ":" + secondsString);
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        time.start();
        dataPanel.add(timeLabel);

        JLabel scoreLabel = new JLabel("Score : ");
        scoreLabel.setSize(w*10 , 20);
        scoreLabel.setMinimumSize(new Dimension(100 , 20));
        scoreLabel.setMaximumSize(new Dimension(100 , 20));

        Thread score = new Thread(()->{
            while(true) {
                scoreLabel.setText("Score : " + rectanglespanel.myBomberMan.score);
            }
        });
        score.start();
        dataPanel.add(scoreLabel);

        add(dataPanel);

        playerPanel = new JPanel();
        playerPanel.setPreferredSize(new Dimension(w*50, 25));
        playerPanel.setMinimumSize(new Dimension(w*50, 25));
        playerPanel.setMaximumSize(new Dimension(w*50, 25));
        playerPanel.setLayout(new BoxLayout(playerPanel , 0));

        JLabel speedLabel = new JLabel("Speed : 2 blocks per s");
        speedLabel.setSize(w*12 , 20);
        speedLabel.setMinimumSize(new Dimension(120 , 20));
        speedLabel.setMaximumSize(new Dimension(120 , 20));

        Thread speed = new Thread(()->{
            while(true) {
                speedLabel.setText("Speed : " + rectanglespanel.myBomberMan.speed +" block/s");
            }
        });
        speed.start();
        playerPanel.add(speedLabel);

        JLabel bombLimitLabel = new JLabel("BombLimit : 1");
        bombLimitLabel.setSize(w*10 , 20);
        bombLimitLabel.setMinimumSize(new Dimension(100 , 20));
        bombLimitLabel.setMaximumSize(new Dimension(100 , 20));
        Thread bombLimit = new Thread(()->{
            while(true) {
                bombLimitLabel.setText("bomb limit : " + rectanglespanel.myBomberMan.bombLimit);
            }
        });
        bombLimit.start();
        playerPanel.add(bombLimitLabel);

        JLabel bombRadiusLabel = new JLabel("Radius : ");
        bombRadiusLabel.setSize(w*8 , 20);
        bombRadiusLabel.setMinimumSize(new Dimension(80 , 20));
        bombRadiusLabel.setMaximumSize(new Dimension(80 , 20));
        Thread bombRadius = new Thread(()->{
            while(true) {
                bombRadiusLabel.setText("Radius : " + rectanglespanel.myBomberMan.bombRadius);
            }
        });
        bombRadius.start();
        playerPanel.add(bombRadiusLabel);

        JLabel ghost = new JLabel("Mode");
        ghost.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/me.png"));
        ghost.setMinimumSize(new Dimension(80 , 20));
        ghost.setMaximumSize(new Dimension(80 , 20));
        Thread ghostCheck = new Thread(()->{
            while(true) {
                if(rectanglespanel.myBomberMan.ghostMode) {

                    ghost.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/spirit.png"));
                }
                else{
                    ghost.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/me.png"));
                }
            }
        });
        ghostCheck.start();
        playerPanel.add(ghost);

        JLabel control = new JLabel();
        control.setSize(w*4 , 20);
        control.setMinimumSize(new Dimension(40 , 20));
        control.setMaximumSize(new Dimension(40 , 20));
        Thread controlCheck = new Thread(()->{
            while(true) {
                if(rectanglespanel.myBomberMan.bombControling) {

                    control.setIcon(new ImageIcon(new File("").getAbsolutePath() + "/images/controlicon.png"));
                }
                else{
                    control.setText("");

                }
            }
        });
        controlCheck.start();
        playerPanel.add(control);
        add(playerPanel);

        add(rectanglespanel);
        pack();
        setLocationRelativeTo(null);
        setSize(w*50 , (h*50)+70);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);


    }

}
